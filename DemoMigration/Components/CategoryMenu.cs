﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DemoMigration.Data;
using DemoMigration.Models;
using Microsoft.AspNetCore.Mvc;

namespace DemoMigration.Components
{
    public class CategoryMenu : ViewComponent
    {
        private readonly DemoDbContext _context;

        public CategoryMenu(DemoDbContext context)
        {
            _context = context;
        }
        public IViewComponentResult Invoke()
        {
         
            var categories = _context.Categories.ToList();
            var categoryMenuViewModel = new CategoryMenuViewModel();
            int i = 0;
            foreach (var category in categories)
            {
                if (i < 4)
                {
                    categoryMenuViewModel.InRaLi.Add(category);
                }

                if (i >= 4)
                {
                    categoryMenuViewModel.DropDownList.Add(category);
                }
                i++;
            }
            return View(categoryMenuViewModel);
        }
    }

    public class CategoryMenuViewModel
    {
        public List<Category> InRaLi { get; set; } = new List<Category>();
        public List<Category> DropDownList { get; set; } = new List<Category>();
    }
}
