﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace DemoMigration.Components
{

    public class PriceMenu:ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            List<string> listPrice = new List<string>()
            {
                "100", "200", "500", "1000"
            };
            return View(listPrice);
        }
    }
}
