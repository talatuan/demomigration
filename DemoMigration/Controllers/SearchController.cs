﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DemoMigration.Data;
using Microsoft.AspNetCore.Mvc;

namespace DemoMigration.Controllers
{
    public class SearchController : Controller
    {
        private readonly DemoDbContext _context;

        public SearchController(DemoDbContext context)
        {
            _context = context;
        }

        public IActionResult SearchByPrice(string price)
        {
            var products = _context.Products.Where(x => x.Price <= Convert.ToDecimal(price)).ToList();
            return View(products);
        }
    }
}
